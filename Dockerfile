FROM docker:stable

SHELL ["/bin/ash", "-e", "-o", "pipefail", "-c"]
RUN apk --no-cache add git \
                       cmake \
                       g++ \
                       gcc \
                       libc-dev \
                       make \
                       ruby-bundler \
                       ruby-dev \
                       ruby-full \
  && rm -rf /var/cache/apk/*

COPY Gemfile* ./

RUN bundle install
